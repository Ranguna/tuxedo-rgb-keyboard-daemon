import numpy as np
from ite8291r3_ctl import ite8291r3
import time
import os
import json
from pathlib import Path
import fasteners
import threading
import importlib
from contextlib import contextmanager
import sys
import traceback
import usb

class Keyboard:
    def __init__(self):
        self.driver = ite8291r3.get()
        self.gamma = (0.55, 0.48, 0.43)

        self.driver.set_brightness(50)
    
    def create_default_colourmap(self, cell_width=18, cell_height=6, cell_value=(1.0,1.0,1.0)):
        return np.ones((cell_height, cell_width, 3), dtype=float) * cell_value

    def colour_to_voltage(self, colour):
        return np.power(colour, 1./np.array(self.gamma))

    def apply_colourmap(self, arr, brightness = 1.0):
        voltmap = self.colour_to_voltage(arr) * brightness
        self.apply_voltmap(voltmap)

    def set_colour(self, colour, brightness = 1.0):
        colourmap = self.create_default_colourmap(cell_value=colour)
        self.apply_colourmap(colourmap, brightness)

    def apply_voltmap(self, voltmap, experimental=True):
        if experimental:
            NUM_ROWS = 6
            NUM_COLS = 21
            ROW_BUFFER_LEN = 3 * NUM_COLS + 2
            ROW_RED_OFFSET   = 1 + 2 * NUM_COLS
            ROW_GREEN_OFFSET = 1 + 1 * NUM_COLS
            ROW_BLUE_OFFSET  = 1 + 0 * NUM_COLS

            arr = np.zeros((NUM_ROWS, ROW_BUFFER_LEN), dtype=np.uint8)
            voltmap_u8 = np.asarray(voltmap*255, dtype=np.uint8)

            arr[:, ROW_RED_OFFSET:ROW_RED_OFFSET+voltmap.shape[1]] = voltmap_u8[:,:,0]
            arr[:, ROW_GREEN_OFFSET:ROW_GREEN_OFFSET+voltmap.shape[1]] = voltmap_u8[:,:,1]
            arr[:, ROW_BLUE_OFFSET:ROW_BLUE_OFFSET+voltmap.shape[1]] = voltmap_u8[:,:,2]

            try:
                self.driver.enable_user_mode()
                for row in range(NUM_ROWS):
                    self.driver._ite8291r3__set_row_index(row)
                    self.driver._ite8291r3__send_data(bytearray(arr[NUM_ROWS-row-1]))
            except usb.core.USBTimeoutError as e:
                print("usb.core.USBTimeoutError occured but trying to recover!")
                usb.util.dispose_resources(self.driver.channel.dev)
                self.driver = ite8291r3.get()
        else:
            itemap = {}
            for i in range(voltmap.shape[0]):
                for j in range(voltmap.shape[1]):
                    itemap[(voltmap.shape[0]-i-1,j)] = tuple( np.asarray(voltmap[i,j]*255, dtype=np.uint8) )
            self.driver.set_key_colours(itemap)

class Daemon:
    def __init__(self):
        self.thread_enabled = False

        self.keyboard = Keyboard()

        self.config_dir = os.path.expanduser('~') + "/.config/tuxedo-keyboard-daemon"
        self.state_file_path = self.config_dir + "/state.json"
        self.custom_scripts_dir = self.config_dir + "/custom"

        self.FIFO = self.config_dir + '/tuxedo-keyboard-rgb-control'
        try:
            os.mkfifo(self.FIFO)
        except FileExistsError:
            pass

        self.load_state()
        self.on_state_change()

    def on_state_change(self):
        if self.state["mode"] == "custom":
            self.stop_active_thread()
            if not Path(self.get_custom_target_py_path()).exists():
                print("Custom script not found.", self.state["target"])
                self.keyboard.set_colour((1.0, 0.0, 0.0), self.state["brightness"])
            else:
                self.start_thread_with_function(self.py_script_function)
        elif self.state["mode"] == 'colour':
            print(self.state)
            self.stop_active_thread()
            self.keyboard.set_colour(
                list(map(lambda x: float(x), self.state["target"].split(","))),
                self.state["brightness"]
            )

    def default_state(self):
        return {
            "mode": "custom",
            "target": "keyboard-mouse",
            "brightness": 1.0,
        }

    def ensure_config_exists(self):
        os.makedirs(self.config_dir, exist_ok=True)
        os.makedirs(self.custom_scripts_dir, exist_ok=True)
        Path(self.state_file_path).touch(exist_ok=True)

    def save_state(self):
        self.ensure_config_exists()
        rw_lock = fasteners.InterProcessReaderWriterLock(self.state_file_path)
        rw_lock.acquire_write_lock()
        try:
            with open(self.state_file_path, "w") as state_file:
                json.dump(self.state, state_file, indent=4)
        finally:
            rw_lock.release_write_lock()

    def load_state(self):
        self.ensure_config_exists()
        rw_lock = fasteners.InterProcessReaderWriterLock(self.state_file_path)
        rw_lock.acquire_read_lock()
        corrupted_state = False
        try:
            with open(self.state_file_path, "r") as state_file:
                try:
                    self.state = json.load(state_file)
                    self.state = {**self.default_state(), **self.state}
                except json.decoder.JSONDecodeError:
                    self.state = self.default_state()
                    corrupted_state = True
        finally:
            rw_lock.release_read_lock()

        if corrupted_state:
            print("State corrupted. Truncating default state")
            self.save_state()

        print("loaded", self.state)
        return self.state

    def start_fifo_command_reading(self):
        print("Reading fifo")
        while True:
            with open(self.FIFO) as fifo:
                for line in fifo:
                    line = line.strip()
                    print("Got command")
                    print("> ", line)
                    try:
                        if line == "kill":
                            self.stop_active_thread()
                            self.keyboard.set_colour((1.0, 1.0, 1.0), self.state["brightness"])
                            self.save_state()
                            sys.exit(0)
                        elif line.startswith("mode"):
                            self.state["mode"], self.state["target"] = line.split(" ")[1:]
                            self.save_state()
                            self.on_state_change()
                        elif line.startswith("brightness"):
                            self.state["brightness"] = float(line.split(" ")[-1])
                            self.save_state()
                            self.on_state_change()
                    except Exception as e:
                        traceback.print_exc()

    def stop_active_thread(self):
        if self.thread_enabled:
            self.thread_enabled = False
            self.active_thread.join()

    def start_thread_with_function(self, function):
        self.stop_active_thread()
        self.thread_enabled = True
        self.active_thread = threading.Thread(target=function)
        self.active_thread.start()

    def get_custom_target_py_path(self):
        return self.custom_scripts_dir + "/" + self.state["target"] + ".py"

    def py_script_function(self):
        print("starting py_script_function")

        spec = importlib.util.spec_from_file_location("module." + self.state["target"], self.get_custom_target_py_path())
        selected_module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(selected_module)

        arr = self.keyboard.create_default_colourmap()
        custom_effect = selected_module.CustomEffect(arr, self)

        while custom_effect.is_enabled() and self.thread_enabled:
            arr = custom_effect.update()

            if arr is None:
                pass
            else:
                self.keyboard.apply_colourmap(arr, self.state["brightness"])

            time.sleep(1/custom_effect.get_fps())

        self.thread_enabled = False
        custom_effect.on_exit()
        print("exiting py_script_function")

if __name__ == "__main__":
    daemon = Daemon()
    daemon.start_fifo_command_reading()
